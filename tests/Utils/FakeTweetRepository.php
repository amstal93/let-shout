<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 24/01/18
 * Time: 15:31
 */

namespace App\Tests\Utils;


use App\Twitter\Domain\Model\Tweet\Tweet;
use App\Twitter\Domain\Model\Tweet\TweetRepository;
use App\Twitter\Domain\Model\Tweet\TweetUsername;

class FakeTweetRepository implements TweetRepository
{
    /**
     * @param TweetUsername $username
     * @param int $total
     * @return array|null
     */
    public function tweetsByUsername(TweetUsername $username, int $total)
    {
        $collection = [];
        for($i = 0; $i < $total; ++$i) {
            $collection[] = [
                'created_at' => 'Thu Oct 19 09:29:31 +0000 2017',
                'id' => 920944993833013248,
                'text' => '#pam2017 https://t.co/uxkDglPHqs',
                'user' => [
                    'screen_name' => 'pacopepe',
                    'profile_image_url' => 'http://pbs.twimg.com/profile_images/2594197339/C1cWmHye_normal'
                ]
            ];
        }

        return $collection;
    }
}