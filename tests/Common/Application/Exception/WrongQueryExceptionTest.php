<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 24/01/18
 * Time: 15:03
 */

namespace App\Tests\Common\Application\Exception;

use App\Tests\Utils\EmptyTweetRepository;
use App\Tests\Utils\SpyQuery;
use App\Twitter\Application\Query\Tweet\TweetShoutQueryHandler;
use PHPUnit\Framework\TestCase;

class WrongQueryExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function errorMessageHasCorrectClassNameExpected()
    {
        try{
            (new TweetShoutQueryHandler(new EmptyTweetRepository()))->handle(new SpyQuery());
        }catch (\Exception $e){
            $this->assertEquals('TweetShoutQueryHandler can only handle TweetShoutQuery', $e->getMessage());
        }
    }
}




