<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:51
 */

namespace App\Tests\Twitter\Domain\Model;

use App\Twitter\Domain\Model\Tweet\TweetId;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class TweetIdTest extends TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \DomainException
     */
    public function testShouldNotCreateWithEmptyId()
    {
        new TweetId('');
    }

    /**
     * @expectedException \DomainException
     */
    public function testShouldNotCreateWithString()
    {
        new TweetId($this->faker->text('15'));
    }

    public function testGetValueShouldReturnTheId()
    {
        $expected = $this->faker->numberBetween(1,10000000);
        $id = new TweetId($expected);
        $this->assertEquals($expected, $id->getValue());
        $this->assertEquals($expected, (string) $id);
    }
}
