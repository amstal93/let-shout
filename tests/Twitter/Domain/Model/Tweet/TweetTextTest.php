<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:51
 */

namespace App\Tests\Twitter\Domain\Model;

use App\Twitter\Domain\Model\Tweet\TweetText;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class TweetTextTest extends TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \DomainException
     */
    public function testShouldNotCreateWithEmptyString()
    {
        new TweetText('');
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedIfItExceedsTheMaximumAllowed()
    {
        $text = $this->faker->paragraph(10);
        new TweetText($text);
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedIfItDoesNotMeetTheMinimumAllowed()
    {
        $text = 'ab';
        new TweetText($text);
    }

    public function testGetValueShouldReturnTheText()
    {
        $expected = $this->faker->text(280);
        $text = new TweetText($expected);
        $this->assertEquals($expected, $text->getValue());
        $this->assertEquals($expected, (string) $text);
    }
}
