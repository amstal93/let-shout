<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 31/01/18
 * Time: 16:00
 */

namespace App\Tests\Twitter\Infrastructure\Application\Service;

use App\Twitter\Infrastructure\Application\Service\ApiExchange;
use App\Twitter\Infrastructure\Application\Service\TwitterApiExchange;
use App\Twitter\Infrastructure\Application\Service\TwitterApiSettings;
use PHPUnit\Framework\TestCase;

class TwitterApiExchangeTest extends TestCase
{
    /**
     * @var TwitterApiExchange
     */
    protected $exchange;

    protected function setUp()
    {
        $token = 'oAccessToken';
        $tokenSecret = 'oAccessTokenSecret';
        $key = 'cKey';
        $secret = 'cSecret';
        $url = 'url';

        $this->exchange = new TwitterApiExchange(
            new TwitterApiSettings($token, $tokenSecret, $key, $secret, $url)
        );
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function constsProvider(){
        $class = new \ReflectionClass(TwitterApiExchange::class);
        return [
            [$class, 'POST', 'POST'],
            [$class, 'GET', 'GET'],
        ];
    }

    public function testsExistsClassTwitterApiExchange()
    {
        $this->assertTrue(class_exists(get_class($this->exchange)));
    }

    public function testsInstanceOfTwitterAPIExchange()
    {
        $this->assertInstanceOf(TwitterAPIExchange::class, $this->exchange);
    }

    /**
     * @param $class \ReflectionClass
     * @param $constExpected string
     * @param $constValueExpected string
     * @dataProvider constsProvider
     * @throws \ReflectionException
     */
    public function testsServiceHasConsts($class, $constExpected, $constValueExpected)
    {
        $this->assertArrayHasKey($constExpected, $class->getConstants());
        $this->assertEquals($constValueExpected, $class->getConstant($constExpected));
    }

    /**
     * @throws \ReflectionException
     */
    public function testConstructorReceivesApiExchangeParameter()
    {
        $class = new \ReflectionClass(TwitterApiExchange::class);
        $parameters = $class->getConstructor()->getParameters();

        $this->assertNotEmpty($parameters, 'Constructor does not receive parameters');
        $this->assertEquals(ApiExchange::class, $parameters[0]->getType());
    }
}
