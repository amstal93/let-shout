<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 31/01/18
 * Time: 17:36
 */

namespace App\Tests\Twitter\Application\Query\Tweet;


use App\Tests\Utils\EmptyTweetRepository;
use App\Tests\Utils\FakeTweetRepository;
use App\Twitter\Application\Query\Tweet\TweetShoutQuery;
use App\Twitter\Application\Query\Tweet\TweetShoutQueryHandler;
use PHPUnit\Framework\TestCase;

class TweetShoutQueryHandlerTest extends TestCase
{
    /**
     * @var EmptyTweetRepository
     */
    private $tweetRepository;

    /**
     * @var TweetShoutQueryHandler
     */
    private $queryHandler;

    protected function setUp()
    {
        $this->tweetRepository = new EmptyTweetRepository();
    }

    /**
     * @expectedException \App\Twitter\Application\Exception\UsernameIsEmptyException
     */
    public function testsThrowExceptionIfUsernameIsEmpty()
    {
        $this->initEmptyQueryHandler();

        $query = new TweetShoutQuery('', 1);
        $this->queryHandler->handle($query);
    }

    /**
     * @expectedException \App\Twitter\Application\Exception\UsernameNotFoundException
     */
    public function testsThrowExceptionIfResponseFromTwitterByUsernameIsEmpty()
    {
        $this->initEmptyQueryHandler();

        $query = new TweetShoutQuery('pacopepeNoexist', 1);
        $this->queryHandler->handle($query);
    }

    private function initEmptyQueryHandler(): void
    {
        $this->queryHandler = new TweetShoutQueryHandler(
            $this->tweetRepository
        );
    }

    public function testsMustReturnResponseIfTheUsernameFinds()
    {
        $this->initFakeQueryHandler();

        $rnd = random_int(1,10);
        $query = new TweetShoutQuery('pacopepe', $rnd);
        $result = $this->queryHandler->handle($query);

        $this->assertTrue(is_array($result));
        $this->assertCount($rnd, $result);
    }

    private function initFakeQueryHandler(): void
    {
        $this->queryHandler = new TweetShoutQueryHandler(
            new FakeTweetRepository()
        );
    }
}