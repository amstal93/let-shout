<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 24/01/18
 * Time: 13:08
 */

namespace App\Common\Application\Exception;

/**
 * Class WrongQueryException
 * @package App\Common\Application\Exception
 */
class WrongQueryException extends \Exception
{
    private const QUERY = "";
    private const HANDLER = "Handler";
    private const MY_MESSAGE = "%s can only handle %s";

    protected $message;

    /**
     * WrongQueryException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->setMessage($class);

        parent::__construct($this->message, 0, null);
    }

    private function setMessage($class)
    {
        try {
            $class = (new \ReflectionClass($class))->getShortName();
            if (false != strpos($class, self::HANDLER)) {
                $className = str_replace(self::HANDLER, self::QUERY, $class);
                $this->message = sprintf(self::MY_MESSAGE, $class, $className);
            }
        } catch (\ReflectionException $e) {
            $this->message = $e->getMessage();
        }
    }
}