<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 2/01/18
 * Time: 13:49
 */

namespace App\Common\Application\Query;


/**
 * Interface CommandHandler
 * @package App\Application\Query
 */
interface QueryHandler
{
    /**
     * @param Query $command
     * @return mixed
     */
    public function handle(Query $command);
}