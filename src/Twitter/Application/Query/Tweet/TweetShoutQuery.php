<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 2/01/18
 * Time: 13:07
 */

namespace App\Twitter\Application\Query\Tweet;

use App\Common\Application\Query\Query;

/**
 * Class TweetShoutCommand
 * @package App\Twitter\Application\Query\User
 */
class TweetShoutQuery implements Query
{
    private $username;
    private $maxTweets;

    /**
     * UserRegisterCommand constructor.
     * @param $username
     * @param $maxTweets
     */
    public function __construct($username, $maxTweets)
    {
        $this->username = $username;
        $this->maxTweets = $maxTweets;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getMaxTweets()
    {
        return $this->maxTweets;
    }
}