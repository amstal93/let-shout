<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 14:03
 */

namespace App\Twitter\Infrastructure\Application\Service;


class TwitterApiSettings implements ApiExchange
{
    /**
     * @var string
     */
    private $oAccessToken;
    /**
     * @var string
     */
    private $oAccessTokenSecret;
    /**
     * @var string
     */
    private $cKey;
    /**
     * @var string
     */
    private $cSecret;
    /**
     * @var string
     */
    private $url;

    /**
     * TwitterApiSettings constructor.
     * @param string $oAccessToken
     * @param string $oAccessTokenSecret
     * @param string $cKey
     * @param string $cSecret
     * @param string $url
     */
    public function __construct(
        string $oAccessToken,
        string $oAccessTokenSecret,
        string $cKey,
        string $cSecret,
        string $url
    ){
        $this->oAccessToken = $oAccessToken;
        $this->oAccessTokenSecret = $oAccessTokenSecret;
        $this->cKey = $cKey;
        $this->cSecret = $cSecret;
        $this->url = $url;

//        echo '<pre>';print_r([__LINE__,__CLASS__, __METHOD__,$this->getSettings()]);die();
    }

    public function getSettings(): array
    {
        return [
            'oauth_access_token' => $this->oAccessToken,
            'oauth_access_token_secret' => $this->oAccessTokenSecret,
            'consumer_key' => $this->cKey,
            'consumer_secret' => $this->cSecret
        ];
    }

    public function getUrlBase(): string
    {
        return $this->url;
    }
}