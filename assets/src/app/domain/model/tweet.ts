export class Tweet {
    public id: number;
    public username: string;
    public text: string;
    public createdAt: string;
    public usernameImage: string;
}